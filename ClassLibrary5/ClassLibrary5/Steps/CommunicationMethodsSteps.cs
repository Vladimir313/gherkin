﻿using System;
using TechTalk.SpecFlow;

namespace Project.Steps
{
    [Binding]
    public class CommunicationMethodsSteps
    {
        [Given(@"User is logined")]
        public void GivenUserIsLogined()
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"User clicks on contact us button")]
        public void WhenUserClicksOnContactUsButton()
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"User clicks on inform about fault link")]
        public void WhenUserClicksOnInformAboutFaultLink()
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"User types Hello in message field in opened pop up")]
        public void WhenUserTypesHelloInMessageFieldInOpenedPopUp()
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"User clicks on send button")]
        public void WhenUserClicksOnSendButton()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"The message is succesfully sent and the pop up disappears")]
        public void ThenTheMessageIsSuccesfullySentAndThePopUpDisappears()
        {
            ScenarioContext.Current.Pending();
        }
    }
}
