﻿using System;
using TechTalk.SpecFlow;

namespace Project.Steps
{
    [Binding]
    public class SearchFieldSteps
    {
        [When(@"User enters ASUS VivoBook X(.*)LI-BQ(.*) in the search field")]
        public void WhenUserEntersASUSVivoBookXLI_BQInTheSearchField(int p0, int p1)
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"User clicks on search button")]
        public void WhenUserClicksOnSearchButton()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"The page of ASUS VivoBook X(.*)LI-BQ(.*) is open")]
        public void ThenThePageOfASUSVivoBookXLI_BQIsOpen(int p0, int p1)
        {
            ScenarioContext.Current.Pending();
        }
    }
}
