﻿using System;
using TechTalk.SpecFlow;

namespace Project.Steps
{
    [Binding]
    public class SubscriptionSteps
    {
        [When(@"User enters rvn(.*)@i\.ua into the email for subscription field")]
        public void WhenUserEntersRvnI_UaIntoTheEmailForSubscriptionField(int p0)
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"User clicks on subscribe button")]
        public void WhenUserClicksOnSubscribeButton()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"User can see a massage of succesful subscription")]
        public void ThenUserCanSeeAMassageOfSuccesfulSubscription()
        {
            ScenarioContext.Current.Pending();
        }
    }
}
