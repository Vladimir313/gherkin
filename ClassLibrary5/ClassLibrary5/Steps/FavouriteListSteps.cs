﻿using System;
using TechTalk.SpecFlow;

namespace Project.Steps
{
    [Binding]
    public class FavouriteListSteps
    {
        [Given(@"ASUS VivoBook X571LI-BQ043 page is open")]
        public void GivenASUSVivoBookXLI_BQPageIsOpen()
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"User Clicks on favourite button")]
        public void WhenUserClicksOnFavouriteButton()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"The pop up with a message of successful adding appears")]
        public void ThenThePopUpWithAMessageOfSuccessfulAddingAppears()
        {
            ScenarioContext.Current.Pending();
        }
    }
}
