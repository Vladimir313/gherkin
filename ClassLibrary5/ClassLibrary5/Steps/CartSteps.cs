﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using Project.POM;
using System;
using TechTalk.SpecFlow;

namespace Project.Steps
{
    [Binding]
    public class CartSteps
    {
        public IWebDriver driver;
        public MainPage cartPopUp;

        public object CartPopUp { get; private set; }
        public MainPage MainPage { get; private set; }

        [BeforeScenario]
        public void CreateDriver()
        {
            driver = new ChromeDriver(@"C:\Users\Владимир\Desktop\ClassLibrary5\packages\Selenium.WebDriver.ChromeDriver.91.0.4472.10100\driver\win32");
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMinutes(20);
        }

        [Given(@"Allo website is open")]
        public void GivenAlloWebsiteIsOpen()
        {
            driver.Navigate().GoToUrl("https://allo.ua");
            driver.Manage().Window.Maximize();
            MainPage = new MainPage(driver);
            CartPopUp = new CartPopUp(driver);
        }
        
        //[Given(@"User is not logged in")]
        //public void GivenUserIsNotLoggedIn()
        //{
        //    string enterAccountTitle = cartPopUp.GetTextFromUserBlockButton();
        //    Assert.AreEqual("Вхід", enterAccountTitle);
        //}

        [When(@"User cliks on cart")]
        public void WhenUserCliksOnCart()
        {
            MainPage.ClickOnCart();
        }

        [Then(@"The cart is empty")]
        public void ThenTheCartIsEmpty()
        {
            string cartEmptyTitle = cartPopUp.GetTextFromCartEmptyTitle();
            Assert.AreEqual("Ваш кошик порожній.", cartEmptyTitle);
        }

        [Given(@"The Ноутбук ASUS VivoBook X571LI-BQ043 page is open")]
        public void GivenTheНоутбукASUSVivoBookXLI_BQPageIsOpen()
        {
            ScenarioContext.Current.Pending();
        }

        [Given(@"User is logged in")]
        public void GivenUserIsLoggedIn()
        {
            ScenarioContext.Current.Pending();
        }

        [When(@"User clicks on buy button")]
        public void WhenUserClicksOnBuyButton()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"User can see added product in appeared cart pop up")]
        public void ThenUserCanSeeAddedProductInAppearedCartPopUp()
        {
            ScenarioContext.Current.Pending();
        }

    }
}
