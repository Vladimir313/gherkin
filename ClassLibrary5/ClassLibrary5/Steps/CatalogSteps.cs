﻿using System;
using TechTalk.SpecFlow;

namespace Project.Steps
{
    [Binding]
    public class CatalogSteps
    {
        [When(@"User clicks on catalog button")]
        public void WhenUserClicksOnCatalogButton()
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"User places the cursor on Смартфони та телефони")]
        public void WhenUserPlacesTheCursorOnСмартфониТаТелефони()
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"User clicks on Смартфони in drop down list")]
        public void WhenUserClicksOnСмартфониInDropDownList()
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"User clicks on Xiaomi Redmi Note 10 Pro 6/128 Onyx Gray")]
        public void WhenUserClicksOnXiaomiRedmiNoteProOnyxGray()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"The page of smartphon Xiaomi Redmi Note 10 Pro 6/128 Onyx Gray is open")]
        public void ThenThePageOfSmartphonXiaomiRedmiNoteProOnyxGrayIsOpen()
        {
            ScenarioContext.Current.Pending();
        }
    }
}
