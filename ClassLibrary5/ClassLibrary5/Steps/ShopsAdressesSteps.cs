﻿using System;
using TechTalk.SpecFlow;

namespace Project.Steps
{
    [Binding]
    public class ShopsAdressesSteps
    {
        [Given(@"Selected city is kiev by default")]
        public void GivenSelectedCityIsKievByDefault()
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"User clicks on shops button")]
        public void WhenUserClicksOnShopsButton()
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"User clicks on Днепр link")]
        public void WhenUserClicksOnДнепрLink()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"User can see addresses of shops in his city")]
        public void ThenUserCanSeeAddressesOfShopsInHisCity()
        {
            ScenarioContext.Current.Pending();
        }
    }
}
