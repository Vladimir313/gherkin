﻿using System;
using TechTalk.SpecFlow;

namespace Project.Steps
{
    [Binding]
    public class CitySelectionSteps
    {
        [When(@"User clicks on Днепр button")]
        public void WhenUserClicksOnДнепрButton()
        {
            ScenarioContext.Current.Pending();
        }
    }
}
