﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using Project.POM;
using System;
using TechTalk.SpecFlow;

namespace Project.Steps
{
    [Binding]
    public class LoginSteps
    {
        public IWebDriver driver;

        public object CartPopUp { get; private set; }
        public EnterUserPopUp EnterUserPopUp { get; private set; }

        [BeforeScenario]
        public void CreateDriver()
        {
            driver = new ChromeDriver(@"C:\Users\Владимир\Desktop\ClassLibrary5\packages\Selenium.WebDriver.ChromeDriver.91.0.4472.10100\driver\win32");
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMinutes(20);
        }

        [Given(@"Allo website is open")]
        public void GivenAlloWebsiteIsOpen()
        {
            driver.Navigate().GoToUrl("https://allo.ua");
            driver.Manage().Window.Maximize();
            EnterUserPopUp = new EnterUserPopUp(driver);
        }
        //[Given(@"User is not registrated")]
        //public void GivenUserIsNotRegistrated()
        //{
        //    ScenarioContext.Current.Pending();
        //}
        
        [When(@"User clicks on user block")]
        public void WhenUserClicksOnUserBlock()
        {
            MainPage.ClickUserBlockButton();
        }
        
        [When(@"User enters rvn(.*)@i\.ua into the phone or e-mail field")]
        public void WhenUserEntersRvnI_UaIntoThePhoneOrE_MailField(int p0)
        {
            EnterUserPopUp.PhoneOrEmailField("rvn2007@i.ua");
        }
        
        [When(@"User enters qwerasdfzxcv into the password field")]
        public void WhenUserEntersQwerasdfzxcvIntoThePasswordField()
        {
            EnterUserPopUp.EnterPasswodField("qwerasdfzxcv");
        }
        
        [When(@"User clicks on enterButton")]
        public void WhenUserClicksOnEnterButton()
        {
            mainPage = EnterUserPopUp.ClickOnEnterButtonInEnterUserPopUp();
        }
        
        [Then(@"User can see his registration name in user block")]
        public void ThenUserCanSeeHisRegistrationNameInUserBlock()
        {
            string userNameLabel = MainPage.GetTextFromUserNameLabel();
            Assert.AreEqual("Владимир", userNameLabel);
        }
    }
}
