﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.POM
{
    public class MainPage
    {
        private IWebDriver _driver;
        private Actions _actions;
        public By mainLabel = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/a/img[1]");

        //header
        public By userBlockButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[5]/div");
        public By userNameLabel = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[5]/div/div/div[1]/span");
        public By enterAccountTitle = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[5]/div/div/button[1]");
        public By cart = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[6]/div/div[1]");
        public By searchField = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[4]/div/div/form/label/input");
        public By searchButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[4]/div/div/form/button/svg");
        public By catalogIcon = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/div/div[1]/p");

        public static void ClickUserBlockButton()
        {
            throw new NotImplementedException();
        }

        public string GetTextFromUserBlockButton()
        {
            throw new NotImplementedException();
        }

        public By catalogButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/div/div[1]/svg/use//svg/path");
        public By catalogSmartphonesAndPhonesDropDownLine = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/div/div[2]" +
            "/div/ul/li[1]/a");

        public static void ClickOnCart()
        {
            throw new NotImplementedException();
        }

        public string GetTextFromCartEmptyTitle()
        {
            throw new NotImplementedException();
        }

        public By smartphonesButtonInDropDownList = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/div/div[2]/div/ul/li[1]" +
            "/div/ul[1]/li[1]/a/p");

        public string GetTextFromUserNameLabel()
        {
            throw new NotImplementedException();
        }

        public By chooseCityButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[1]/div");
        public By chooseCityTitle = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[1]/div[2]/h3");
        public By closeChooseingCitylist = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[1]/div[2]/svg/use//svg/path");
        public By chooseKievButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[1]/div[2]/ul/li[1]/span");
        public By chooseKharkovButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[1]/div[2]/ul/li[2]/span");
        public By chooseOdessaButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[1]/div[2]/ul/li[3]/span");
        public By chooseDneprButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[1]/div[2]/ul/li[4]/span");
        public By chooseLvovButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[1]/div[2]/ul/li[5]/span");
        public By chooseZaporozhieButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[1]/div[2]/ul/li[6]/span");
        public By inputCityField = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[1]/div[2]/div/div/div/input");

        public By chooseDarkTheme = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[2]/span[1]");
        public By chooseLightTheme = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[2]/span[2]");
        public By blogButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[1]/a");
        public By fishkaButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[2]/a");
        public By vacanciesButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[3]/a");
        public By shopButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[4]/a");
        public By deliveryAndPaymentButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[5]/a");
        public By creditButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[6]/a");
        public By guarantiesButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[7]/a");
        public By contactsButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[8]/a");
        public By languagetsButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[4]/span[2]/div");
        public By alloSmartLiveLink = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/ul/li[1]/a");
        public By alloMoneyProgramLink = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/ul/li[2]/a");
        public By alloUpgradeProgramLink = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/ul/li[3]/a");
        public By alloChangeLink = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/ul/li[4]/a");
        public By alloMarkdownLink = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/ul/li[5]/a");
        public By alloPromotionsLink = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/ul/li[6]/a");

        //communication methods
        public By contactUsButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[2]/div[2]/svg");
        public By telephonLink = By.XPath("/html/body/div[1]/div/div/div[1]/div[2]/div[1]/a[1]");
        public By telegramLink = By.XPath("/html/body/div[1]/div/div/div[1]/div[2]/div[1]/a[2]");
        public By viberLink = By.XPath("/html/body/div[1]/div/div/div[1]/div[2]/div[1]/a[3]");
        public By messangerLink = By.XPath("/html/body/div[1]/div/div/div[1]/div[2]/div[1]/a[4]");
        public By foultMessageLink = By.XPath("/html/body/div[1]/div/div/div[1]/div[2]/div[1]/a[5]");
        public By closeCommunicationButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[2]/div[2]");

        //subscribe block
        public By emailForSubscribtionField = By.XPath("/html/body/div[1]/div/div/div[3]/div/div[1]/div/div/div/div/div[1]/input");
        public By subscribeButton = By.XPath("/html/body/div[1]/div/div/div[3]/div/div[1]/div/div/div/button");
        public By thanksGivingMAssage = By.XPath("/html/body/div[1]/div/div/div[3]/div/div[1]/div/h3[1]");
        public By subscribeMAssage = By.XPath("/html/body/div[1]/div/div/div[3]/div/div[1]/div/div/p");

        public MainPage(IWebDriver driver)
        {
            this._driver = driver;
            _actions = new Actions(driver);
        }
        // переход в корзину
        
        
        public CartPopUp ClickOnСart()
        {
            _driver.FindElement(cart).Click();
            return new CartPopUp(_driver);
        }
        // получение текста из титула пользовательского блока когда пользователь зарегистрирован
        public IWebElement FindUserNameLabel()
        {
            return _driver.FindElement(userNameLabel);
        }
        public string UserNameLabel()
        {
            return FindUserNameLabel().Text;

        }
        // проверка того, что пользователь не зарегистрирован
        public IWebElement FindEnterAccountTitle()
        {
            return _driver.FindElement(enterAccountTitle);
        }
        public string EnterAccountTitle()
        {
            return FindEnterAccountTitle().Text;

        }

        // поиск кнопки связи с продавцом
        public MainPage ClickOnContactUsButton()
        {
            _driver.FindElement(contactUsButton).Click();
            return this;
        }
        public FaultMessageRopUp ClickOnFoultMessageLink()
        {
            _driver.FindElement(foultMessageLink).Click();
            return new FaultMessageRopUp(_driver);
        }
        // переход в окно связи с продавцом
        public ShopsAddressesPage ClickOnShopButton()
        {
            _driver.FindElement(shopButton).Click();
            return new ShopsAddressesPage(_driver);
        }
        // поиск товара через каталог
        public MainPage ClickOnCatalogButton()
        {
            _driver.FindElement(catalogButton).Click();
            return this;
        }
        // переход через выпадающий список на страницу смартфонов
        public SmartphonesPage ClickOnCatalogSmartphonesAndPhonesDropDownLine()
        {
            IWebElement button = _driver.FindElement(catalogSmartphonesAndPhonesDropDownLine);
            _actions.MoveToElement(_driver.FindElement(By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/div/div[2]/div/ul/li[1]")));
            _actions.Perform();
            button.Click();
            return new SmartphonesPage(_driver);
        }
        // поиск и клик по кнопки выбора города
        public MainPage ClickOnChooseCityButton()
        {
            _driver.FindElement(chooseCityButton).Click();
            return this;
        }
        // выбор города
        public MainPage ClickOnСhooseDneprButton()
        {
            _driver.FindElement(chooseDneprButton).Click();
            return this;
        }
        //получение текста из названия города
        public IWebElement FindСhooseCityTitle()
        {
            return _driver.FindElement(chooseCityTitle);
        }
        public string СhooseCityTitle()
        {
            return FindСhooseCityTitle().Text;
        }
        // поиск товара через строку поиска
        public MainPage EnterDataToSearchField(string name)
        {
            _driver.FindElement(searchField).SendKeys(name);
            return this;
        }
        public ASUSVivoBookX571LIBQ043 ClickOnSearchButton()
        {
            _driver.FindElement(shopButton).Click();
            return new ASUSVivoBookX571LIBQ043(_driver);
        }
        // подписка на рассылку
        public MainPage EnterDataToУmailForSubscribtionField(string email)
        {
            _driver.FindElement(emailForSubscribtionField).SendKeys(email);
            return this;
        }
        public MainPage ClickOnSubscribeButton()
        {
            _driver.FindElement(subscribeButton).Click();
            return this;
        }
        // проверка успешной подписки
        public IWebElement FindThanksGivingMAssage()
        {
            return _driver.FindElement(thanksGivingMAssage);
        }
        public string ThanksGivingMAssage()
        {
            return FindThanksGivingMAssage().Text;
        }

        public IWebElement FindSubscribeMAssage()
        {
            return _driver.FindElement(subscribeMAssage);
        }
        public string SubscribeMAssage()
        {
            return FindSubscribeMAssage().Text;
        }
    }
}

