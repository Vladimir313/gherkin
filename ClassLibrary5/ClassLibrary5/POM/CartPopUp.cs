﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.POM
{
    public class CartPopUp
    {
        private IWebDriver _driver;
        private Actions _actions;

        public By cartHeaderTitle = By.XPath("/html/body/div[3]/div/div/div[2]/span");
        public By cartEmptyTitle = By.XPath("/html/body/div[3]/div/div/div[2]/span");
        public By closeCartButton = By.XPath("/html/body/div[3]/div/div/div[1]/svg");
        public By cartBackLink = By.XPath("/html/body/div[3]/div/div/div[3]/div/div[1]/div/p[2]/a");

        public By deleteProductButton = By.XPath ("/html/body/div[3]/div/div/div[3]/div/div[1]/div/div/div[1]/div/ul/li/div/div[1]" +
            "/div[2]/div[1]/svg");
        public By productImage = By.XPath("/html/body/div[3]/div/div/div[3]/div/div[1]/div/div/div[1]/div/ul/li/div/div[1]/div[1]/a/img");
        public By productName = By.XPath("/html/body/div[3]/div/div/div[3]/div/div[1]/div/div/div[1]/div/ul/li/div/div[1]/div[2]" +
            "/div[1]/a/p/span");
        public By producQuantity = By.XPath("/html/body/div[3]/div/div/div[3]/div/div[1]/div/div/div[1]/div/ul/li/div/div[1]/div[2]" +
            "/div[2]/div[1]/label/input");
        public By addProductButton = By.XPath("/html/body/div[3]/div/div/div[3]/div/div[1]/div/div/div[1]/div/ul/li/div/div[1]/div[2]" +
            "/div[2]/div[1]/svg[2]/use//svg/path");
        public By subtractProductButton = By.XPath("/html/body/div[3]/div/div/div[3]/div/div[1]/div/div/div[1]/div/ul/li/div/div[1]" +
            "/div[2]/div[2]/div[1]/svg[1]");
        public By priceBox = By.XPath("/html/body/div[3]/div/div/div[3]/div/div[1]/div/div/div[1]/div/ul/li/div/div[1]/div[2]" +
            "/div[2]/div[2]");
        public By extraServiseTitle = By.XPath("/html/body/div[3]/div/div/div[3]/div/div[1]/div/div/div[1]/div/ul/li/div/div[2]/h4");
        public By insuranceAndGuaranteeTitle = By.XPath("/html/body/div[3]/div/div/div[3]/div/div[1]/div/div/div[1]/div/ul/li/div/div[2]" +
            "/ul/li/h5");
        public By extraServicesButton = By.XPath ("/html/body/div[3]/div/div/div[3]/div/div[1]/div/div/div[1]/div/ul/li/div/div[2]" +
            "/button/span");
        public By extraServicesDropDownButton = By.XPath("/html/body/div[3]/div/div/div[3]/div/div[1]/div/div/div[1]" +
            "/div/ul/li/div/div[2]/button/i");
        public By scrollBarr = By.XPath("/html/body/div[3]/div/div/div[3]/div/div[1]/div/div/div[3]/div");
        public By productServicesList = By.XPath("/html/body/div[3]/div/div/div[3]/div/div[1]/div/div/div[1]/div/ul/li/div/div[2]" +
            "/ul/li[1]/ul");
        public By softwareAndServicesTitle = By.XPath("/html/body/div[3]/div/div/div[3]/div/div[1]/div/div/div[1]/div/ul/li/div/div[2]" +
            "/ul/li[2]/h5");
        public By softwareServiceBlock = By.XPath("/html/body/div[3]/div/div/div[3]/div/div[1]/div/div/div[1]/div/ul/li/div/div[2]" +
            "/ul/li[2]/ul/li/div");
        public By servicesGroupTitle = By.XPath("/html/body/div[3]/div/div/div[3]/div/div[1]/div/div/div[1]/div/ul/li/div/div[2]" +
            "/ul/li[3]/h5");
        public By servicesGroupBlock = By.XPath("/html/body/div[3]/div/div/div[3]/div/div[1]/div/div/div[1]/div/ul/li/div/div[2]" +
            "/ul/li[3]/ul/li/div");
        public By backToChoseButton = By.XPath("/html/body/div[3]/div/div/div[3]/div/div[1]/footer/button");
        public By totalTitle = By.XPath("/html/body/div[3]/div/div/div[3]/div/div[1]/footer/div/p/span[1]");
        public By totalPriceBox = By.XPath("/html/body/div[3]/div/div/div[3]/div/div[1]/footer/div/p/span[2]");
        public By issueTheOrderButton = By.XPath("/html/body/div[3]/div/div/div[3]/div/div[1]/footer/div/button");

        internal string GetTextFromUserBlockButton()
        {
            throw new NotImplementedException();
        }

        public CartPopUp(IWebDriver driver)
        {
            this._driver = driver;
        }
        //поиск элементов и текста на странице пустой корзины
        public IWebElement FindCartHeaderTitle()
        {
            return _driver.FindElement(cartHeaderTitle);
        }

        public string GetTextFromCartHeaderTitle()
        {
            return FindCartHeaderTitle().Text;
        }

        public IWebElement FindCartEmptyTitle()
        {
            return _driver.FindElement(cartEmptyTitle);
        }

        public string GetTextFromCartEmptyTitle()
        {
            return FindCartEmptyTitle().Text;
        }
                
        public CartPopUp ClickOnCartBackLink()
        {
            _driver.FindElement(cartBackLink).Click();
            return this;
        }

        public CartPopUp ClickOnCloseCartButton()
        {
            _driver.FindElement(closeCartButton).Click();
            return this;
        }
        // поиск текста названия доьавленного в корзину товара
        public IWebElement FindProductName()
        {
            return _driver.FindElement(productName);
        }

        public string GetTextFromproductName()
        {
            return FindProductName().Text;
        }

    }
}
