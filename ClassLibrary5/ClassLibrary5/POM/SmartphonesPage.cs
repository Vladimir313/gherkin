﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.POM
{
    public class SmartphonesPage
    {
        private IWebDriver _driver;
        private Actions _actions;

        public By smartphoneXiomiRedmiNote10Pro = By.XPath("/html/body/div[1]/div/div/div[2]/div[2]/div[1]/div[2]/div[3]/div[1]" +
            "/div/div[2]/div/div/div[1]/div[2]/div/div/a[2]/picture/img");
        private IWebDriver driver;

        public SmartphonesPage(IWebDriver driver)
        {
            this._driver = driver;
            _actions = new Actions(driver);
        }
        public XiaomiRedmiNote10Pro ClickOnSmartphoneXiomiRedmiNote10Pro()
        {
            _driver.FindElement(smartphoneXiomiRedmiNote10Pro).Click();
            return new XiaomiRedmiNote10Pro(_driver);
        }
    }
}
