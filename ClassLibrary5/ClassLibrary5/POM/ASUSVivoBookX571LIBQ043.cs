﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.POM
{
    public class ASUSVivoBookX571LIBQ043
    {
        private IWebDriver _driver;
        private Actions _actions;

        public By asusVivoBookX571LIBQ043BuyButton = By.XPath("/html/body/div[1]/div/div/div[2]/div[1]/div[2]/div[2]/main/div[5]/div[1]" +
            "/button[1]");
        public By asusVivoBookX571LIBQ043FavouriteButton = By.XPath("/html/body/div[1]/div/div/div[2]/div[1]/div[2]/div[2]/main/div[5]" +
            "/div[2]/div[4]");
        public By asusVivoBookX571LIBQ043Title = By.XPath("/html/body/div[1]/div/div/div[2]/div[1]/div[1]/h1");
        public By asusVivoBookX571LIBQ043Image = By.XPath("/html/body/div[1]/div/div/div[2]/div[1]/div[2]/div[2]/main/div[2]" +
            "/div/div[2]/div/div/div/div[1]/div/picture/img");

        public ASUSVivoBookX571LIBQ043(IWebDriver driver)
        {
            this._driver = driver;
            _actions = new Actions(driver);
        }
        // добавление товара в корзину
        public CartPopUp ClickOnAsusVivoBookX571LIBQ043BuyButton()
        {
            _driver.FindElement(asusVivoBookX571LIBQ043BuyButton).Click();
            return new CartPopUp(_driver);
        }
        public FavouritesPopUp ClickOnAsusVivoBookX571LIBQ043FavouriteButton()
        {
            _driver.FindElement(asusVivoBookX571LIBQ043FavouriteButton).Click();
            return new FavouritesPopUp(_driver);
        }
        // проверка того, что мы на странице с нужным товаром
        public IWebElement FindAsusVivoBookX571LIBQ043Title()
        {
            return _driver.FindElement(asusVivoBookX571LIBQ043Title);
        }
        public string GetTextFromAsusVivoBookX571LIBQ043Title()
        {
            return FindAsusVivoBookX571LIBQ043Title().Text;
        }
        // поиск изображения на странице
        public IWebElement FindAsusVivoBookX571LIBQ043Image()
        {
            return _driver.FindElement(asusVivoBookX571LIBQ043Image);
        }
        //проверка отображения изображения товара
        public string CheckTheAsusVivoBookX571LIBQ043Image()

        {
            return FindAsusVivoBookX571LIBQ043Image().GetAttribute("src");
        }
    }
}
