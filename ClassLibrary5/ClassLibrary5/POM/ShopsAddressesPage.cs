﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.POM
{
    public class ShopsAddressesPage
    {
        private IWebDriver _driver;
        private Actions _actions;

        public By addressesOfShopsTitle = By.XPath("/html/body/div[1]/div/div/div[2]/div[1]/div[1]/h1");
        public By enterYourCityTitle = By.XPath("/html/body/div[1]/div/div/div[2]/div[1]/div[1]/div/label");
        public By cityNameField = By.XPath("/html/body/div[1]/div/div/div[2]/div[1]/div[1]/div/div[1]/div/input");
        public By dneprCityLink = By.XPath("/html/body/div[1]/div/div/div[2]/div[1]/ul/li[1]/a");
        public By shopsAddressesMap = By.XPath("/html/body/div[1]/div/div/div[2]/div[1]/div[2]/div[1]/img");
        public By shopsAddressesList = By.XPath("/html/body/div[1]/div/div/div[2]/div[1]/div[3]/div");
        private IWebDriver driver;

        public ShopsAddressesPage(IWebDriver driver)
        {
            this.driver = driver;
        }
        public ShopsAddressesPage ClickOnDneprCityLink()
        {
            _driver.FindElement(dneprCityLink).Click();
            return this;
        }
        //найти карту
        public IWebElement FindShopsAddressesMap()
        {
            return _driver.FindElement(shopsAddressesMap);
        }
        //проверка отображения карты
        public string CheckShopsAddressesMap()
        {
            return FindShopsAddressesMap().GetAttribute("src");
        }
    }
}
