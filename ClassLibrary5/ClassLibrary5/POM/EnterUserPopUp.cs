﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.POM
{
    public class EnterUserPopUp
    {
        private IWebDriver _driver;
        private Actions actions;
        private Actions _actions;

        public By phoneOrEmailField = By.XPath("/html/body/div[3]/div/div/div[2]/div/div/div[1]/div[1]/form/div/div[1]/div[1]" +
            "/div[1]/input");
        public By enterPasswodField = By.XPath("/html/body/div[3]/div/div/div[2]/div/div/div[1]/div[1]/form/div/div[1]/div[1]" +
            "/div[1]/input");
        public By enterButton = By.XPath("/html/body/div[3]/div/div/div[2]/div/div/div[1]/div[1]/form/div/button");
        public By registrationButton = By.XPath("/html/body/div[3]/div/div/div[2]/div/ul/li[2]");
        public By nameField = By.XPath("/html/body/div[3]/div/div/div[2]/div/div/div[1]/form/div/div[1]/div[1]/input");
        public By phoneField = By.XPath("/html/body/div[3]/div/div/div[2]/div/div/div[1]/form/div/div[2]/div[1]/input");
        public By emailField = By.XPath("/html/body/div[3]/div/div/div[2]/div/div/div[1]/div[1]/form/div/div[1]/div[1]/div[1]/input");
        public By passwordField = By.XPath("/html/body/div[3]/div/div/div[2]/div/div/div[1]/div[1]/form/div/div[1]/div[2]/div[1]" +
            "/input");
        public By getRegistratedButton = By.XPath("/html/body/div[3]/div/div/div[2]/div/div/div[1]/form/button");

        internal void PhoneOrEmailField(string v)
        {
            throw new NotImplementedException();
        }

        public EnterUserPopUp(IWebDriver driver)
        {
            this._driver = driver;
            actions = new Actions(driver);
        }

        internal void EnterPasswodField(string v)
        {
            throw new NotImplementedException();
        }

        internal object ClickOnEnterButtonInEnterUserPopUp()
        {
            throw new NotImplementedException();
        }

        // поиск поля для ввода телефона и ввод телефона
        public EnterUserPopUp EnterDataToPhoneOrEmailField(string phone)
        {
            _driver.FindElement(phoneOrEmailField).SendKeys(phone);
            return this;
        }
        //поиск поля для ввода пароля и ввод пароля
        public EnterUserPopUp EnterDataToEnterPasswodField(string password)
        {
            _driver.FindElement(enterPasswodField).SendKeys(password);
            return this;
        }
        // поиск кнопки входа в аккаунт о вход в аккаунт
        public MainPage ClickOnEnterButton()
        {
            _driver.FindElement(enterButton).Click();
            return new MainPage(_driver);
        }
    }
}
