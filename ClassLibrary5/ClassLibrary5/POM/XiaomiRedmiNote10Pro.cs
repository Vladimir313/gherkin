﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.POM
{
    public class XiaomiRedmiNote10Pro
    {
        private IWebDriver _driver;
        private Actions _actions;

        public By xiaomiRedmiNote10ProTitle = By.XPath("/html/body/div[1]/div/div/div[2]/div[1]/div[1]/h1");
        public By xiaomiRedmiNote10ProImage = By.XPath("/html/body/div[1]/div/div/div[2]/div[1]/div[2]/div[2]/main/div[2]" +
            "/div/div[2]/div/div/div/div[1]/div/picture/img");
        public By XiomiRedmiNote10ProBuyButton = By.XPath("/html/body/div[1]/div/div/div[2]/div[1]/div[2]/div[2]/main/div[5]/div[1]" +
            "/button[1]");
        public By XiomiRedmiNote10ProFavouriteButton = By.XPath("/html/body/div[1]/div/div/div[2]/div[1]/div[2]/div[2]/main/div[5]" +
            "/div[2]/div[4]");
        private IWebDriver driver;

        public XiaomiRedmiNote10Pro(IWebDriver driver)
        {
            this._driver = driver;
            _actions = new Actions(driver);
        }
        // проверка того, что мы находимся на странице с выбранным товаром
        public IWebElement FindXiaomiRedmiNote10ProTitle()
        {
            return _driver.FindElement(xiaomiRedmiNote10ProTitle);
        }
        public string GetTextFromXiaomiRedmiNote10ProTitle()
        {
            return FindXiaomiRedmiNote10ProTitle().Text;
        }
        // поиск изображения на странице
        public IWebElement FindXiaomiRedmiNote10ProImage()
        {
            return _driver.FindElement(xiaomiRedmiNote10ProImage);
        }
        //проверка отображения изображения товара
        public string CheckTheXiaomiRedmiNote10ProImage()

        {
            return FindXiaomiRedmiNote10ProImage().GetAttribute("src");
        }
        
    }
}
