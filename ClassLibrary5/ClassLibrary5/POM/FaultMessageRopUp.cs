﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.POM
{
    public class FaultMessageRopUp
    {
        private IWebDriver _driver;
        private Actions _actions;

        public By informAboutFaultTitle = By.XPath("/html/body/div[3]/div/div/div[2]/div/div/p[1]");
        public By closeMessagePopUpButton = By.XPath ("/html/body/div[3]/div/div/div[1]/svg");
        public By instructionMessagePopUpText = By.XPath("/html/body/div[3]/div/div/div[2]/div/div/p[2]");
        public By messagePopUpUserBlock = By.XPath("/html/body/div[3]/div/div/div[2]/div/div/p[3]");
        public By messagePopUpUserEmail = By.XPath("/html/body/div[3]/div/div/div[2]/div/div/div[1]/div/input");
        public By messagePopUpMessageField = By.XPath("/html/body/div[3]/div/div/div[2]/div/div/div[1]/div/input");
        public By sendMessageButton = By.XPath("/html/body/div[3]/div/div/div[2]/div/div/button");
        public By enterwithAnotherNameLink = By.XPath("/html/body/div[3]/div/div/div[2]/div/div/p[4]/span");
        private IWebDriver driver;

        public FaultMessageRopUp(IWebDriver driver)
        {
            this._driver = driver;
            _actions = new Actions(driver);
        }
        // отправить сообщение продавцу
        public FaultMessageRopUp EnterTextToMessagePopUpMessageField(string message)
        {
            _driver.FindElement(messagePopUpMessageField).SendKeys(message);
            return this;
        }
        public MainPage ClickOnCloseMessagePopUpButton()
        {
            _driver.FindElement(closeMessagePopUpButton).Click();
            return new MainPage(_driver);
        }
    }
}
