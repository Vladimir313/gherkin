﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.POM
{
    public class FavouritesPopUp
    {
        private IWebDriver _driver;
        private Actions _actions;

        public By favouritesPopUpIcon = By.XPath("/html/body/div[3]/div/div/div[2]/div/svg");
        public By favouritesPopUpCloseButton = By.XPath("/html/body/div[3]/div/div/div[1]/svg/use//svg/path");
        public By favouritesPopUpText = By.XPath("/html/body/div[3]/div/div/div[2]/div/p");
        public By favouritesLink = By.XPath("/html/body/div[3]/div/div/div[2]/div/p/a");

        public FavouritesPopUp(IWebDriver driver)
        {
            this._driver = driver;
            _actions = new Actions(driver);
        }
        // проверка подтверждения добавления товара в избранное
        public IWebElement FindFavouritesPopUpText()
        {
            return _driver.FindElement(favouritesPopUpText);
        }

        public string GetTextFromFavouritesPopUpText()
        {
            return FindFavouritesPopUpText().Text;
        }
        // закрытие окошка с сообщением о добавлении товара
        public FavouritesPopUp ClickOnFavouritesPopUpCloseButton()
        {
            _driver.FindElement(favouritesPopUpCloseButton).Click();
            return this;
        }

    }
}
