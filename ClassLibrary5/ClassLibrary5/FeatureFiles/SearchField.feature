﻿Feature: SearchField
	As a user
	I want to use searching field 
	In order to find products that I am interested in faster

@mytag
Scenario: Unautorized user finds the page of ASUS VivoBook X571LI-BQ043
	Given Allo website is open
	And User is not logged in
	When User enters ASUS VivoBook X571LI-BQ043 in the search field
	And User clicks on search button
	Then The page of ASUS VivoBook X571LI-BQ043 is open