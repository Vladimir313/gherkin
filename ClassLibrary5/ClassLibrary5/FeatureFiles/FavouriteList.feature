﻿Feature: FavouriteList
	As a user
	I want to add some products to a favourite list
	In order to find them fast later

@mytag
Scenario:Autorized user adds some products into his favourite list
	Given User is logined
	And ASUS VivoBook X571LI-BQ043 page is open 
	When User Clicks on favourite button
	Then The pop up with a message of successful adding appears