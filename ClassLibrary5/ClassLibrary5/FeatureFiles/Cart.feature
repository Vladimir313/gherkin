﻿Feature: Cart
	As a user
	I want to have access to my cart
	In order to see a contant of my order any time

	As a user
	I want to add products to my cart
	for haveing oportunity to see a contant of my order any time

@mytag
Scenario: Unautorized user has an empty cart
	Given Allo website is open
	And User is not logged in
	When User cliks on cart
	Then The cart is empty

	Scenario: Autorized user adds some products to his cart
	Given User is logged in 
	And The Ноутбук ASUS VivoBook X571LI-BQ043 page is open
	When User clicks on buy button
	Then User can see added product in appeared cart pop up