﻿Feature: ShopsAdresses
	As a user
	I want to see addresses of Allo shops on the map
	In order find the most convenient adress faster

@mytag
Scenario: Unautorized user finds addresses of shops in his city
	Given Allo website is open
	And User is not logged in
	And Selected city is kiev by default
	When User clicks on shops button
	And User clicks on Днепр link
	Then User can see addresses of shops in his city