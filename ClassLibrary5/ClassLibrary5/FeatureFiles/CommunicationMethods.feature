﻿Feature: CommunicationMethods
	As a user
	I want to see all communication methods
	In order to choose one to contact with the seller

@mytag
Scenario: Autorized user sends the message about fault
	Given Allo website is open
	And User is logined
	When User clicks on contact us button
	And User clicks on inform about fault link
	And User types Hello in message field in opened pop up
	And User clicks on send button
	Then The message is succesfully sent and the pop up disappears