﻿Feature: Catalog
	As a user
	I want to use catalog
	In order to see some products and choose that I need

@mytag
Scenario: Unautorized user opens the page with smartphon Xiaomi Redmi Note 10 Pro 6/128 Onyx Gray is open
	Given Allo website is open
	And User is not logged in
	When User clicks on catalog button
	And User places the cursor on Смартфони та телефони
	And User clicks on Смартфони in drop down list
	And User clicks on Xiaomi Redmi Note 10 Pro 6/128 Onyx Gray
	Then The page of smartphon Xiaomi Redmi Note 10 Pro 6/128 Onyx Gray is open
