﻿Feature: Subscription
	As a user
	I want to send my e-mail
	In order to get news about promotions

@mytag
Scenario: Unautorized user subscribes to news about promotions
	Given Allo website is open
	And User is not logged in
	When User enters rvn2008@i.ua into the email for subscription field
	And User clicks on subscribe button
	Then User can see a massage of succesful subscription