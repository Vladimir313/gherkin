﻿Feature: CitySelection
	As a user
	I want to choose my city name in sity list
	In order to find addresses of shops in my city later

@mytag
Scenario: Unautorized user chooses his city
	Given Allo website is open
	And User is not logged in
	And The city name is Киев by default
	When User clicks on city button
	And User clicks on Днепр button 
	Then The name of user's city is displayed on city button